import { IBuilder } from './builder.interface';
import { BuildersLoader } from './default/builder.loader';
import { INestApplication } from '@nestjs/common';

export class BuilderRegistry {

  public static instance(): BuilderRegistry {
    if (this.thisObject) {
      return this.thisObject;
    }
    this.thisObject = new BuilderRegistry();
    return this.thisObject;
  }

  private static thisObject: BuilderRegistry;
  private builders: IBuilder[] = [];
  private static options: object;

  private constructor() {
    BuildersLoader.loadAllBuilders(this);
  }

  public registerBuilder(builder: IBuilder): void {
    this.builders.push(builder);
  }

  public getAllBuilders(): IBuilder[] {
    return this.builders;
  }

  public buildAll(app: INestApplication, options = {}) {
    const arr = [];
    this.getAllBuilders().forEach ((builder) => {
      builder.build(app, options);
      arr.push(builder);
    });
    return arr;
  }
}
