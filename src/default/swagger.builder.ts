import 'reflect-metadata';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { IBuilder } from '../builder.interface';
import { BuilderRegistry } from '../builder.registry';
import { INestApplication } from '@nestjs/common';

export class SwaggerBuilder implements IBuilder {

  public static instance(builderRegistry: BuilderRegistry): IBuilder {
    if (this.thisObject) {
      return this.thisObject;
    }
    this.thisObject = new SwaggerBuilder();
    builderRegistry.registerBuilder(this.thisObject);
    return this.thisObject;
  }

  private static thisObject: SwaggerBuilder;
  private constructor() { }

  public build(app: INestApplication, option) {
    const swaggerOptions = option.swaggerOptions || {};
    const options = new DocumentBuilder()
      .setTitle(swaggerOptions.title || 'NestJs Example')
      .setDescription(swaggerOptions.description || 'NestJs Api Description')
      .setVersion(swaggerOptions.version || '1.0')
      .addTag(swaggerOptions.tag || 'NestJsProject')
      .addBearerAuth()
      .build();
    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup('api', app, document);
    return true;
  }
}
