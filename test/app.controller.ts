import { Get, Controller } from '@nestjs/common';

@Controller()
export class AppController {
  public constructor() { }

    @Get()
  public async findAll() {
    return { accepted: true }
  }
}